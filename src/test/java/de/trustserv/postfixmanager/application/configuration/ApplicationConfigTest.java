package de.trustserv.postfixmanager.application.configuration;

import de.trustserv.postfixmanager.infrastructure.portsadapters.out.postfix.PostfixManagerTest;
import org.junit.Test;

import java.net.URL;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import static org.hamcrest.core.IsEqual.equalTo;

public class ApplicationConfigTest {

    private final URL resource = PostfixManagerTest.class.getClass().getResource("main.cf.example");

    @Test
    public void canCreate() throws Exception {
        ApplicationConfig.getInstance();
    }

    @Test
    public void isSingleton() throws Exception {
        ApplicationConfig a = ApplicationConfig.getInstance();
        ApplicationConfig b = ApplicationConfig.getInstance();
        assertThat(a, is(equalTo(b)));
    }

//    @Test
//    public void getPostfixManager() throws Exception {
//        final PostfixManager postfixManager = ApplicationConfig.getInstance().getPostfixManager(resource.getFile());
//        assertThat(postfixManager, is(notNullValue()));
//    }
}