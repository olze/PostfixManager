package de.trustserv.postfixmanager.infrastructure.portsadapters.out.postfix;

import org.junit.Test;

import java.net.URL;

public class PostfixManagerTest {


    private final URL resource = getClass().getResource("main.cf.example");

    @Test(expected = NullPointerException.class)
    public void canCreateNPE() throws Exception {
        new PostfixManager(null);
    }

    @Test
    public void canCreate() throws Exception {
        new PostfixManager(resource.getFile());
    }

//    @Test
//    public void getAliasMapsInfo() throws Exception {
//        final PostfixManager postfixManager = new PostfixManager(resource.getFile());
//        PostfixAliasMapsInfo postfixAliasMapsInfo = postfixManager.getAliasMapsInfo();
//        assertThat(postfixAliasMapsInfo.getFilePath(), equalTo("mysql-virtual-alias-maps.cf"));
//    }
}