package de.trustserv.postfixmanager.infrastructure.portsadapters.out.postfix;

import org.junit.Test;
import org.mockito.Mockito;

public class PostfixAliasMapsTest {

    //    final PostfixAliasMaps mapsInfo = new PostfixAliasMaps(new PostfixAliasMapsInfo("mysql", fileLocation));
    final PostfixAliasMaps mapsInfo = Mockito.mock(PostfixAliasMaps.class);

    @Test(expected = NullPointerException.class)
    public void canCreate() throws Exception {
        new PostfixAliasMaps(null);
    }

//    @Test
//    public void execute() throws Exception {
//        final URL resource = getClass().getResource("mysql-virtual-alias-maps.cf");
//        final String fileLocation = resource.getFile();
//
//        ResultSet resultSet = mapsInfo.executeQuery("olze@trustserv.de");
//        assertThat(resultSet, is(notNullValue()));
//    }
}