package de.trustserv.postfixmanager.infrastructure.portsadapters.out.dovecot;

import org.junit.Test;

import java.net.URL;

public class DovecotConfTest {

    final URL resource = getClass().getResource("dovecot-sql.conf.ext");

    @Test
    public void canCreate() throws Exception {
        new DovecotConf(resource.getFile());
    }

//    @Test
//    public void canParse() throws Exception {
//        final DovecotConf dovecotConf = new DovecotConf(resource.getFile());
//        dovecotConf.parse();
//        assertThat(dovecotConf.getDriver(), is("mysql"));
//        assertThat(dovecotConf.getConnect(), is("host=127.0.0.1 dbname=postfixadmin user=someuser password=longsecretpassword"));
//        assertThat(dovecotConf.getConnectHost(), is("127.0.0.1"));
//        assertThat(dovecotConf.getConnectDbname(), is("postfixadmin"));
//        assertThat(dovecotConf.getConnectUser(), is("someuser"));
//        assertThat(dovecotConf.getConnectPassword(), is("longsecretpassword"));
//        assertThat(dovecotConf.getDefaultPassScheme(), is("SHA512-CRYPT"));
//        assertThat(dovecotConf.getPasswordQuery(), is("SELECT password,CONCAT('/var/vmail/', maildir) AS userdb_home,\\\n" +
//                "    '5000' AS userdb_uid, '5000' AS userdb_gid,\\\n" +
//                "    concat('*:bytes=', quota) AS userdb_quota_rule\\\n" +
//                "    FROM mailbox WHERE username='%u' AND domain='%d' AND active=1"));
//        assertThat(dovecotConf.getUserQuery(), is("SELECT CONCAT('/var/vmail/', maildir) AS home, 'maildir:~/' as mail, '5000' AS uid, '5000' AS gid,\\\n" +
//                "    concat('*:bytes=', quota) AS quota_rule\\\n" +
//                "    FROM mailbox WHERE username='%u' AND domain='%d' AND active=1"));
//
//    }
}