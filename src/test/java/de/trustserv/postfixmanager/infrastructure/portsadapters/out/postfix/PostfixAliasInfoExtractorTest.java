package de.trustserv.postfixmanager.infrastructure.portsadapters.out.postfix;

import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;

import static de.trustserv.postfixmanager.infrastructure.portsadapters.out.postfix.PostfixAliasInfoEnums.QUERY;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.notNullValue;

public class PostfixAliasInfoExtractorTest {
    private final File resource = new File(getClass().getResource("mysql-virtual-alias-maps.cf").getFile());

    @Test
    public void canCreate() throws FileNotFoundException {
        new PostfixAliasInfoExtractor(resource);
    }

    @Test
    public void canExtract() throws Exception {
        PostfixAliasInfoExtractor extractor = new PostfixAliasInfoExtractor(resource);
        String query = extractor.extract(QUERY);
        assertThat(query, is(notNullValue()));
        assertThat(query, is("SELECT goto FROM alias WHERE address='%s' AND active = '1'"));
    }
}