package de.trustserv.postfixmanager.common.domain.model;

/**
 *
 * @author oli
 * @param <T>
 */
public interface Repository<T extends Entity> {

  T get(String mail);

  void save(T aggregate);
}
