package de.trustserv.postfixmanager.application.services;

import de.trustserv.postfixmanagerapi.rest.commands.unauthorized.RegisterUserCommand;
import de.trustserv.postfixmanagerapi.rest.commands.unauthorized.RequestCaptchaCommand;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

class CaptchaDB {

    private final static Set<CaptchaDBEntry> dbEntries = new HashSet<>();
    private Comparator<CaptchaDBEntry> dbEntryComparator = Comparator.comparing(CaptchaDBEntry::getRequestTime);

    private long getRequestCounts(RequestCaptchaCommand cmd) {

        return dbEntries.stream()
                .filter(dbEntry -> dbEntry.getIp().equals(cmd.getIp()) &&
                        dbEntry.getBrowserfingerprint().equals(cmd.getBrowserFingerprint()))
                .findAny()
                .map(CaptchaDBEntry::getRequestCounts)
                .orElse(0L);
    }

    boolean isAllowedToRequestCaptcha(RequestCaptchaCommand cmd) {

        final Optional<CaptchaDBEntry> matchingEntryForRequestCommand = findMatchingEntryForRequestCommand(cmd);

        if (!matchingEntryForRequestCommand.isPresent()) {
            //no request yet saved for that ip/browser, allow it
            return true;
        }

        if (findMatchingEntryForIP(cmd.getIp()) > 5) {
            //thats wierd! 5 ips means 5 different browsers! attack?
            return false;
        }

        final CaptchaDBEntry captchaDBEntry = matchingEntryForRequestCommand.get();
        if (captchaDBEntry.getRequestCounts() >= 5) {
            final LocalDateTime requestTime = captchaDBEntry.getRequestTime();
            if (LocalDateTime.now().minus(10, ChronoUnit.SECONDS).isBefore(requestTime)) {
                return false;
            }
        }

        return true;
    }

    private long findMatchingEntryForIP(String ip) {
        return dbEntries.stream().filter(captchaDBEntry -> captchaDBEntry.getIp().equals(ip)).count();
    }

    private Optional<CaptchaDBEntry> findMatchingEntryForRequestCommand(RequestCaptchaCommand cmd) {
        return findMatchingEntryFor(cmd.getIp(), cmd.getBrowserFingerprint());
    }

    private Optional<CaptchaDBEntry> findMatchingEntryFor(String ip, String browserfingerprint) {
        for (CaptchaDBEntry dbEntry : dbEntries) {
            if (dbEntry.getIp().equals(ip)
                    && dbEntry.getBrowserfingerprint().equals(browserfingerprint)) {
                return Optional.of(dbEntry);
            }
        }
        return Optional.empty();
    }


    void updateOrSaveCaptchaRequest(RequestCaptchaCommand cmd, String token) {
        final CaptchaDBEntry captchaDBEntry = new CaptchaDBEntry(cmd.getIp(), cmd.getBrowserFingerprint(), getRequestCounts(cmd) + 1, LocalDateTime.now(), token);
        removeCaptchaEntryIfExists(captchaDBEntry);
        saveCaptchaEntry(captchaDBEntry);
    }

    private void saveCaptchaEntry(CaptchaDBEntry captchaDBEntry) {
        final boolean saved = dbEntries.add(captchaDBEntry);
        if (!saved) {
            throw new RuntimeException("Unable to store captcha registration " + captchaDBEntry);
        }
    }

    private void removeCaptchaEntryIfExists(CaptchaDBEntry captchaDBEntry) {
        if (dbEntries.contains(captchaDBEntry)) {
            final boolean removed = dbEntries.remove(captchaDBEntry);
            if (!removed) {
                throw new RuntimeException("Unable to remove captcha registration " + captchaDBEntry);
            }
        }
    }

    public boolean captchaCorrect(RegisterUserCommand command) {
        final Optional<CaptchaDBEntry> dbEntry = findMatchingEntryForRegisterUserCommand(command);
        return dbEntry.map(captchaDBEntry -> captchaDBEntry.getTokenToEnter().equals(command.getCaptcha())).orElse(false);
    }

    private Optional<CaptchaDBEntry> findMatchingEntryForRegisterUserCommand(RegisterUserCommand command) {
        return findMatchingEntryFor(command.getIp(), command.getBrowserFingerprint());
    }

    private class CaptchaDBEntry {
        private final String ip;
        private final String browserfingerprint;
        private final long requestCounter;
        private final LocalDateTime requestTime;
        private final String tokenToEnter;

        CaptchaDBEntry(String ip, String browserfingerprint, long requestCounter, LocalDateTime requestTime, String token) {
            this.ip = ip;
            this.browserfingerprint = browserfingerprint;
            this.requestCounter = requestCounter;
            this.requestTime = requestTime;
            this.tokenToEnter = token;
        }

        public String getTokenToEnter() {
            return tokenToEnter;
        }

        String getIp() {
            return ip;
        }

        public String getBrowserfingerprint() {
            return browserfingerprint;
        }

        long getRequestCounts() {
            return requestCounter;
        }

        LocalDateTime getRequestTime() {
            return requestTime;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            CaptchaDBEntry dbEntry = (CaptchaDBEntry) o;

            if (!ip.equals(dbEntry.ip)) return false;
            return browserfingerprint.equals(dbEntry.browserfingerprint);
        }

        @Override
        public int hashCode() {
            int result = ip.hashCode();
            result = 31 * result + browserfingerprint.hashCode();
            return result;
        }

    }


}
