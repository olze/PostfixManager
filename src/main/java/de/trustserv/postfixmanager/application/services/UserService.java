
package de.trustserv.postfixmanager.application.services;


import de.trustserv.postfixmanagerapi.rest.commands.authorized.DeleteUserCommand;
import de.trustserv.postfixmanagerapi.rest.exceptions.register.InvalidCaptcha;
import de.trustserv.postfixmanagerapi.rest.commands.unauthorized.LoginUserCommand;
import de.trustserv.postfixmanagerapi.rest.commands.unauthorized.RegisterUserCommand;
import de.trustserv.postfixmanagerapi.rest.exceptions.register.RegistrationAlreadyWaitingForApproval;

import javax.ws.rs.NotAllowedException;

public interface UserService {
    /**
     * Register a new user.
     *
     * @param command The command containing the payload.
     * @return A String representing the new user.
     * @throws de.trustserv.postfixmanagerapi.rest.exceptions.register.InvalidCaptcha
     * @throws de.trustserv.postfixmanagerapi.rest.exceptions.register.RegistrationAlreadyWaitingForApproval
     */
    String registerUser(RegisterUserCommand command) throws InvalidCaptcha, RegistrationAlreadyWaitingForApproval;

    void deleteUser(DeleteUserCommand command) throws NotAllowedException;

    String loginUserReturnToken(LoginUserCommand command) throws AuthenticationException;
}
