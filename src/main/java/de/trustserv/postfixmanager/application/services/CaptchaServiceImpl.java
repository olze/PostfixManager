package de.trustserv.postfixmanager.application.services;

import com.github.cage.Cage;
import com.github.cage.GCage;
import de.trustserv.postfixmanager.infrastructure.portsadapters.out.captcha.CaptchaService;
import de.trustserv.postfixmanagerapi.rest.commands.unauthorized.RegisterUserCommand;
import de.trustserv.postfixmanagerapi.rest.commands.unauthorized.RequestCaptchaCommand;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.core.Response;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class CaptchaServiceImpl implements CaptchaService {

    private final static CaptchaDB captchaDB = new CaptchaDB();
    private final Logger logger = LogManager.getLogger(this.getClass());

    @Override
    public Response requestCaptcha(RequestCaptchaCommand cmd) throws TooManyRequestsException {

        if (!captchaDB.isAllowedToRequestCaptcha(cmd)) {
            logger.debug(() -> "not allowed to request captcha: " + cmd.toString());
            throw new TooManyRequestsException();
        }

        final Cage cage = new GCage();
        final ByteArrayOutputStream os = new ByteArrayOutputStream();
        final String token = writeImageToStream(cage, os);
        closeSilently(os);
        persistCaptchaRequest(cmd, token);

        return Response.ok(os.toByteArray()).build();
    }

    @Override
    public boolean captchaCorrect(RegisterUserCommand command) {
        return captchaDB.captchaCorrect(command);
    }

    private void persistCaptchaRequest(RequestCaptchaCommand cmd, String token) {
        captchaDB.updateOrSaveCaptchaRequest(cmd, token);
    }

    private String writeImageToStream(Cage cage, ByteArrayOutputStream os) {
        final String token;
        try {
            token = cage.getTokenGenerator().next();
            cage.draw(token, os);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
        logger.debug(() -> "created captcha with token " + token);
        return token;
    }

    private void closeSilently(ByteArrayOutputStream os) {
        try {
            os.close();
        } catch (IOException e) {
            //ignore
        }
    }


}
