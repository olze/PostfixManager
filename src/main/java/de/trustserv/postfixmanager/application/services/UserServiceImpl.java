package de.trustserv.postfixmanager.application.services;

import de.trustserv.postfixmanager.domain.model.encryption.SecurityProvider;
import de.trustserv.postfixmanager.domain.model.registration.Registration;
import de.trustserv.postfixmanager.domain.model.registration.RegistrationRepository;
import de.trustserv.postfixmanager.domain.model.user.User;
import de.trustserv.postfixmanager.domain.model.user.UserRepository;
import de.trustserv.postfixmanager.infrastructure.portsadapters.out.captcha.CaptchaService;
import de.trustserv.postfixmanagerapi.rest.commands.authorized.DeleteUserCommand;
import de.trustserv.postfixmanagerapi.rest.exceptions.register.InvalidCaptcha;
import de.trustserv.postfixmanagerapi.rest.commands.unauthorized.LoginUserCommand;
import de.trustserv.postfixmanagerapi.rest.commands.unauthorized.RegisterUserCommand;
import de.trustserv.postfixmanagerapi.rest.exceptions.register.RegistrationAlreadyWaitingForApproval;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.validation.Valid;
import java.time.LocalDateTime;

public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RegistrationRepository registrationRepository;
    private final SecurityProvider securityProvider;
    private final CaptchaService captchaService;

    public UserServiceImpl(UserRepository userRepository, RegistrationRepository registrationRepository,
                           SecurityProvider provider, CaptchaService captchaService) {
        this.userRepository = userRepository;
        this.registrationRepository = registrationRepository;
        this.securityProvider = provider;
        this.captchaService = captchaService;
    }

    @Override
    public String registerUser(@Valid RegisterUserCommand command) throws InvalidCaptcha, RegistrationAlreadyWaitingForApproval {

        if (!captchaService.captchaCorrect(command)) {
            throw new InvalidCaptcha("Invalid Captcha");
        }

        if (registrationRepository.find(command.getUsername()).isPresent()) {
            throw new RegistrationAlreadyWaitingForApproval(command.getUsername());
        }

        registrationRepository.save(new Registration(command.getUsername(), command.getIp(), LocalDateTime.now(), null, null));
        userRepository.save(new User(command.getUsername(), command.getPassword(), command.getName()));
        return command.getUsername();
    }

    @Override
    public void deleteUser(@Valid DeleteUserCommand command) {
        Jwt jwt = Jwts.parser().setSigningKey(securityProvider.getKey()).parse(command.getToken());
        System.out.println(jwt.getBody());

    }

    @Override
    public String loginUserReturnToken(@Valid LoginUserCommand command) {
        userRepository.checkCredentials(command.getUsername(), command.getPassword());
        return Jwts.builder().setSubject(command.getUsername()).signWith(SignatureAlgorithm.RS512, securityProvider.getKey()).compact();
    }
}
