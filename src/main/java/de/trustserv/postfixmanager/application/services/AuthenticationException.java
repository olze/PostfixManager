package de.trustserv.postfixmanager.application.services;

public class AuthenticationException extends Exception {

    public AuthenticationException(String message) {
        super(message);
    }
}
