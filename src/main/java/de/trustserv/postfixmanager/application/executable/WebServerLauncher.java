package de.trustserv.postfixmanager.application.executable;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.jboss.resteasy.plugins.server.servlet.HttpServletDispatcher;

public class WebServerLauncher {

  public static void main(final String args[]) throws Exception {

    final String classNameOfRessources = "de.trustserv.postfixmanager.application.configuration.AppRessourceConfig";
    final String initParam = "javax.ws.rs.Application";
    final ServletHolder servletHolder = new ServletHolder(new HttpServletDispatcher());
    servletHolder.setInitParameter(initParam, classNameOfRessources);

    final ServletContextHandler servletCtxHandler = new ServletContextHandler();
    servletCtxHandler.addServlet(servletHolder, "/");

    final Server server = new Server(8081);
    server.setHandler(servletCtxHandler);
    server.start();
    server.join();
  }
}
