package de.trustserv.postfixmanager.application.configuration;


import de.trustserv.postfixmanager.domain.model.encryption.SecurityProvider;
import de.trustserv.postfixmanager.domain.model.registration.RegistrationRepository;
import de.trustserv.postfixmanager.domain.model.user.UserRepository;
import de.trustserv.postfixmanager.infrastructure.portsadapters.out.encryption.DefaultSecurityProvider;
import de.trustserv.postfixmanager.infrastructure.portsadapters.out.persistence.mysql.MariaDBRegistrationRepository;
import de.trustserv.postfixmanager.infrastructure.portsadapters.out.persistence.mysql.MariaDBUserRepository;
import de.trustserv.postfixmanager.infrastructure.portsadapters.out.postfix.PostfixManager;

public class ApplicationConfig {

    private static ApplicationConfig instance;

    private ApplicationConfig() {
    }

    public static ApplicationConfig getInstance() {
        if (instance == null) {
            instance = new ApplicationConfig();
        }
        return instance;
    }

    public PostfixManager getPostfixManager() {
        return new PostfixManager(getConfigFilePath());
    }

    public UserRepository getUserRepository() {
        return MariaDBUserRepository.getInstance(getPostfixManager());
    }

    public RegistrationRepository getRegistrationRepository() {
        return MariaDBRegistrationRepository.createInstance();
    }

    public SecurityProvider getSecurityProvider() {
        return new DefaultSecurityProvider();
    }

    public String getConfigFilePath() {
        return "/etc/postfix/main.cf";
    }

}
