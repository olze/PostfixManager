package de.trustserv.postfixmanager.application.configuration;

import de.trustserv.postfixmanager.application.services.CaptchaServiceImpl;
import de.trustserv.postfixmanager.application.services.UserService;
import de.trustserv.postfixmanager.application.services.UserServiceImpl;
import de.trustserv.postfixmanager.infrastructure.portsadapters.in.web.rest.SessionRessource;
import de.trustserv.postfixmanager.infrastructure.portsadapters.in.web.rest.UserRessource;
import de.trustserv.postfixmanager.infrastructure.portsadapters.out.captcha.CaptchaService;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/services")
public final class AppRessourceConfig extends Application {

  private final Set<Object> classesSet = new HashSet<>();
  private final ApplicationConfig config = ApplicationConfig.getInstance();

  public AppRessourceConfig() {
      classesSet.add(new UserRessource(getUserService(), getCaptchaService()));
    classesSet.add(new SessionRessource(getUserService()));
  }
  
  public UserService getUserService() {
    return new UserServiceImpl(config.getUserRepository(), config.getRegistrationRepository(), config.getSecurityProvider(), getCaptchaService());
  }

  @Override
  public Set<Object> getSingletons() {
    return classesSet;
  }

    public CaptchaService getCaptchaService() {
        return new CaptchaServiceImpl();
    }
}
