package de.trustserv.postfixmanager.infrastructure.portsadapters.out.persistence.mysql;

import de.trustserv.postfixmanager.domain.model.user.User;
import de.trustserv.postfixmanager.domain.model.user.UserRepository;
import de.trustserv.postfixmanager.infrastructure.portsadapters.out.postfix.PostfixAliasMaps;
import de.trustserv.postfixmanager.infrastructure.portsadapters.out.postfix.PostfixManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MariaDBUserRepository implements UserRepository {

    private final String sqlDriver = "org.mariadb.jdbc.Driver";
    private final File configFile = new File("sqlconfig.properties");
    private final String PROPERTY_SQL_USER = "sql.user";
    private final String PROPERTY_SQL_PASS = "sql.password";
    private final String PROPERTY_SQL_QRY_CREATE_USER = "sql.createQry";
    private final String PROPERTY_SQL_QRY_USER_EXISTS = "sql.userExistsQry";
    private final String PROPERTY_SQL_HOST = "sql.host";
    private final String PROPERTY_SQL_DB = "sql.db";
    private final PostfixManager postfixManager;
    private String username;
    private String password;
    private String host;
    private String db;
    private Connection connection;
    private PreparedStatement createUserStmt;
    private PreparedStatement userExistsStmt;
    private PreparedStatement checkPasswordStmt;

    private MariaDBUserRepository(PostfixManager postfixManager) {
        this.postfixManager = postfixManager;
    }

    public static MariaDBUserRepository getInstance(PostfixManager postfixManager) {
        MariaDBUserRepository mariaDBUserRepository = new MariaDBUserRepository(postfixManager);
        try {
            mariaDBUserRepository.init();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mariaDBUserRepository;
    }

    public void init() throws SQLException {
        final Properties sqlConfig = new Properties();
        try {
            try (FileInputStream fileInputStream = new FileInputStream(configFile)) {
                sqlConfig.load(fileInputStream);
            }
        } catch (IOException ex) {
            throw new SQLException("SQL Config not found at " + configFile.getAbsolutePath(), ex);
        }

        username = sqlConfig.getProperty(PROPERTY_SQL_USER);
        password = sqlConfig.getProperty(PROPERTY_SQL_PASS);
        host = sqlConfig.getProperty(PROPERTY_SQL_HOST);
        db = sqlConfig.getProperty(PROPERTY_SQL_DB);

        final String connectionString = "jdbc:mariadb://" + host + "/" + db;
        try {
            // This will load the MySQL driver, each DB has its own driver
            Class.forName(sqlDriver);
            // Setup the connection with the DB
            connection = DriverManager.getConnection(connectionString, username, password);
            createUserStmt = connection.prepareStatement(sqlConfig.getProperty(PROPERTY_SQL_QRY_CREATE_USER));
            userExistsStmt = connection.prepareStatement(sqlConfig.getProperty(PROPERTY_SQL_QRY_USER_EXISTS));
        } catch (ClassNotFoundException | SQLException ex) {
            throw new SQLException("Unable to connection to the db. Please check settings and the mysql .jar file", ex);
        }
    }

    @Override
    public User get(String mail) {
        ensureConnected();
        return null;
    }

    private void ensureConnected() {
        try {
            if (!connection.isValid(500)) {
                throw new SQLException("Invalid SQL Connection. Forgot to call init() ?");
            }
        } catch (SQLException ex) {
            Logger.getLogger(MariaDBUserRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void save(User aggregate) {
        ensureConnected();
        //TODO: replace the "by the user"-defined variables from the command which we read in
        try {
            createUserStmt.setString(1, aggregate.getMail());
            createUserStmt.setString(2, aggregate.getPassword());
            createUserStmt.setString(3, aggregate.getName());
            createUserStmt.setString(4, aggregate.getMaildir());
            createUserStmt.setString(5, aggregate.getQuota());
            createUserStmt.setString(6, aggregate.getLocalPart());
            createUserStmt.setString(7, aggregate.getDomain());
            createUserStmt.setString(8, LocalDateTime.now().toString());
            createUserStmt.setString(9, LocalDateTime.now().toString());
            createUserStmt.setInt(10, 0);
            createUserStmt.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public int checkCredentials(String username, String password) {
        ensureConnected();
        if (!userExists(username)) {
            return ERROR_UNKNOWN_USER;
        }
        //TODO: get the infos from the dovecot config
//    password_query = SELECT password,CONCAT('/var/vmail/', maildir) AS userdb_home,\
//    '5000' AS userdb_uid, '5000' AS userdb_gid,\
//    concat('*:bytes=', quota) AS userdb_quota_rule\
//    FROM mailbox WHERE username='%u' AND domain='%d' AND active=1

        //TODO: to get the user, we use this one:
//    user_query = SELECT CONCAT('/var/vmail/', maildir) AS home, 'maildir:~/' as mail, '5000' AS uid, '5000' AS gid,\
//    concat('*:bytes=', quota) AS quota_rule\
//    FROM mailbox WHERE username='%u' AND domain='%d' AND active=1

        try {
            checkPasswordStmt.setString(1, username);
            checkPasswordStmt.setString(2, password);
            createUserStmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return VALID_CREDENTIALS;
    }

    private boolean userExists(String username) {
        //garrrr fucking shit, this aint userExists in postfixmanager, i implemented "get the aliases" !!!
        //userExists must be implemented in here, not in fucking postfixmanager?! no wait, its dovecot! fucking dovecot, not postfix!
        final PostfixAliasMaps postfixAliasMaps = postfixManager.getAliasMapsInfo().getPostfixAliasMaps();
        final ResultSet resultSet = postfixAliasMaps.executeQuery(username);
        try {
            if (resultSet.first()) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
