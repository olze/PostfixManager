package de.trustserv.postfixmanager.infrastructure.portsadapters.in.web.rest;

import de.trustserv.postfixmanager.application.services.TooManyRequestsException;
import de.trustserv.postfixmanager.application.services.UserService;
import de.trustserv.postfixmanager.infrastructure.portsadapters.out.captcha.CaptchaService;
import de.trustserv.postfixmanagerapi.rest.exceptions.register.InvalidCaptcha;
import de.trustserv.postfixmanagerapi.rest.commands.unauthorized.RegisterUserCommand;
import de.trustserv.postfixmanagerapi.rest.exceptions.register.RegistrationAlreadyWaitingForApproval;
import de.trustserv.postfixmanagerapi.rest.commands.unauthorized.RequestCaptchaCommand;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/users")
public class UserRessource {

    private final UserService userService;
    private final CaptchaService captchaService;

    public UserRessource(UserService userService, CaptchaService captchaService) {
        this.userService = userService;
        this.captchaService = captchaService;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/")
    public Response registerUser(RegisterUserCommand cmd) {
        final String user;

        try {
            user = userService.registerUser(cmd);
        } catch (InvalidCaptcha e) {
            return Response.status(Response.Status.PRECONDITION_FAILED).build();
        } catch (RegistrationAlreadyWaitingForApproval e) {
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
        return Response.status(Response.Status.CREATED).entity(user).build();
    }

    @POST
    @Path("/captcha")
    @Produces("image/jpeg")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response requestCaptchaForIP(RequestCaptchaCommand cmd) {
        final Response response;
        try {
            response = captchaService.requestCaptcha(cmd);
        } catch (TooManyRequestsException e) {
            return Response.notModified().entity(e.getMessage()).build();
        }
        return response;
    }

}
