package de.trustserv.postfixmanager.infrastructure.portsadapters.out.shared;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import static java.util.Objects.requireNonNull;

public abstract class GenericInfoExtractor<T extends Enum> {
    private final File source;

    public GenericInfoExtractor(File resource) {
        requireNonNull(source = resource);
    }

    public String extract(T info) throws FileNotFoundException, QueryEntryNotFoundException {
        return extractIdentifier(info.name());
    }

    private String extractIdentifier(String nameOfIdentifier) throws FileNotFoundException, QueryEntryNotFoundException {
        final Scanner scanner = new Scanner(source, "UTF-8");
        while (scanner.hasNext()) {
            final String line = scanner.nextLine();
            if (line.trim().isEmpty()) {
                continue;
            }
            if (!line.contains("=")) {
                continue;
            }
            final String split[] = line.split("=", 2);
            if (split[0].trim().equalsIgnoreCase(nameOfIdentifier)) {
                return split[1].trim();
            }
        }
        throw new QueryEntryNotFoundException(nameOfIdentifier, source.getAbsolutePath());
    }


    public static class QueryEntryNotFoundException extends Exception {
        public QueryEntryNotFoundException(String identifier, String filePath) {
            super("Entry " + identifier + " not found in: " + filePath);
        }
    }
}
