package de.trustserv.postfixmanager.infrastructure.portsadapters.out.postfix;

/**
 * This class represents the entry to the alias maps info file in the main.cf,
 * describing the file location and the driver
 */

public final class PostfixAliasMapsInfo {

    //driver, like mysql:
    private final String driver;
    //config for that command, like mysql-virtual-alias-maps.cf
    private final String filePath;
    //business logic
    private final PostfixAliasMaps postfixAliasMaps;

    public PostfixAliasMapsInfo(String driver, String filePath) {
        this.driver = driver;
        this.filePath = filePath;
        postfixAliasMaps = new PostfixAliasMaps(this);
    }

    public String getDriver() {
        return driver;
    }

    public String getFilePath() {
        return filePath;
    }

    public PostfixAliasMaps getPostfixAliasMaps() {
        return postfixAliasMaps;
    }

    public String getFQDriver() {
        return "org.mariadb.jdbc.Driver";
    }
}
