package de.trustserv.postfixmanager.infrastructure.portsadapters.out.dovecot;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import static java.util.Objects.requireNonNull;

public class DovecotConf {

    //points to the configuration
    private final String configFile;

    //fields which get extracted after parsing the configuration
    private String connect;
    private String host;
    private String dbname;
    private String user;
    private String password;
    private String default_pass_scheme;
    private String password_query;
    private String user_query;
    private String driver;
    private String connectHost;
    private String connectDbname;
    private String connectUser;
    private String connectPassword;
    private String defaultPassScheme;
    private String passwordQuery;
    private String userQuery;

    public DovecotConf(String file) {
        requireNonNull(configFile = file);
    }

    public void parse() throws IOException {
        final File config = new File(configFile);
        if (!config.canRead()) {
            throw new FileNotFoundException(configFile);
        }

        Properties props = new Properties();
        props.load(new FileInputStream(config));
        final String user_query = props.getProperty("user_query");
        System.out.print(user_query);

    }

    public String getDriver() {
        return driver;
    }

    public String getConnect() {
        return connect;
    }

    public String getConnectHost() {
        return connectHost;
    }

    public String getConnectDbname() {
        return connectDbname;
    }

    public String getConnectUser() {
        return connectUser;
    }

    public String getConnectPassword() {
        return connectPassword;
    }

    public String getDefaultPassScheme() {
        return defaultPassScheme;
    }

    public String getPasswordQuery() {
        return passwordQuery;
    }

    public String getUserQuery() {
        return userQuery;
    }
}
