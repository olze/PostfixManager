package de.trustserv.postfixmanager.infrastructure.portsadapters.out.shared;

import java.util.Scanner;

public final class GenericConfigEntry {

    private String key;
    private String value;

    public GenericConfigEntry(String s) throws InvalidEntryException {

        //we do not parse comments
        if (s.trim().startsWith("#")) {
            throw new InvalidEntryException(s + " does not contain a '='");
        }

        try (final Scanner lineScanner = new Scanner(s)) {
            lineScanner.useDelimiter("=");
            if (lineScanner.hasNext()) {
                //assumes the line has a certain structure
                key = lineScanner.next().trim();
                value = lineScanner.next().trim();
            }
        }
    }


    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public static class InvalidEntryException extends Exception {
        public InvalidEntryException(String message) {
            super(message);
    }
    }
}
