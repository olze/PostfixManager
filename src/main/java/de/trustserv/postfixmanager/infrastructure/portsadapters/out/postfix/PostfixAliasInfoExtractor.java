package de.trustserv.postfixmanager.infrastructure.portsadapters.out.postfix;

import de.trustserv.postfixmanager.infrastructure.portsadapters.out.shared.GenericInfoExtractor;

import java.io.File;

/**
 * This class extracts information for the PostfixAliasMapsInfo entry, which points to the file which holds the
 * database information to query the aliases.
 */
public final class PostfixAliasInfoExtractor extends GenericInfoExtractor {

    public PostfixAliasInfoExtractor(File resource) {
        super(resource);
    }

}
