package de.trustserv.postfixmanager.infrastructure.portsadapters.out.dovecot;

import de.trustserv.postfixmanager.infrastructure.portsadapters.out.shared.GenericInfoExtractor;

import java.io.File;

public class DovecotInfoExtractor extends GenericInfoExtractor {

    public DovecotInfoExtractor(File resource) {
        super(resource);
    }
}
