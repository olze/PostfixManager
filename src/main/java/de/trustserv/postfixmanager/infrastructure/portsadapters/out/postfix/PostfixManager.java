package de.trustserv.postfixmanager.infrastructure.portsadapters.out.postfix;

import de.trustserv.postfixmanager.infrastructure.portsadapters.out.shared.GenericConfigEntry;
import de.trustserv.postfixmanager.infrastructure.portsadapters.out.shared.GenericConfigEntry.InvalidEntryException;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

import static java.util.Objects.requireNonNull;

public final class PostfixManager {


    private final String configFile;

    /**
     * Default constructor
     *
     * @param configFile for the _main_ postfix configuration, usually /etc/postfix/main.cf
     */
    public PostfixManager(String configFile) {
        requireNonNull(this.configFile = configFile);
    }

    protected PostfixAliasMapsInfo getConfigEntry(PostfixVirtualConfigEntries.ENTRIES entryIdentifier) {
        try {
            try (Scanner fileScanner = new Scanner(new FileInputStream(configFile), "UTF-8")) {
                while (fileScanner.hasNextLine()) {
                    final String nextLine = fileScanner.nextLine();
                    if (nextLine.trim().isEmpty()) {
                        continue;
                    }
                    final GenericConfigEntry entry;
                    try {
                        entry = new GenericConfigEntry(nextLine);
                    } catch (InvalidEntryException e) {
                        //TODO: at the moment we dont care for invalid entries in the config - should be handled in a later
                        continue;
                    }
                    //todo: bring the split logic into the entry
                    if (entry.getKey().equals(entryIdentifier.identifier())) {
                        return getPostfixAliasMapsInfo(entry);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        throw new RuntimeException("Something went wrong! " + entryIdentifier + " not found in " + configFile);
    }

    private PostfixAliasMapsInfo getPostfixAliasMapsInfo(GenericConfigEntry entry) {
        return new PostfixAliasMapsInfo(entry.getValue().split(":")[0], entry.getValue().split(":")[1]);
    }


    public PostfixAliasMapsInfo getAliasMapsInfo() {
            return getConfigEntry(PostfixVirtualConfigEntries.ENTRIES.VIRTUAL_ALIAS_MAPS);
    }
}
