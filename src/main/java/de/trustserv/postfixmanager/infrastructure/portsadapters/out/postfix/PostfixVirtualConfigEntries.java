package de.trustserv.postfixmanager.infrastructure.portsadapters.out.postfix;

public interface PostfixVirtualConfigEntries {

    String CONF_VIRTUAL_ALIAS_MAPS = "virtual_alias_maps";

    enum ENTRIES {
        VIRTUAL_ALIAS_MAPS(CONF_VIRTUAL_ALIAS_MAPS);

        private final String identifier;

        ENTRIES(String identifier) {
            this.identifier = identifier;
        }

        public String identifier() {
            return identifier;
        }
    }

}
