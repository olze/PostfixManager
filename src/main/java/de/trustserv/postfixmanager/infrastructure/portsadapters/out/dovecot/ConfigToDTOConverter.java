package de.trustserv.postfixmanager.infrastructure.portsadapters.out.dovecot;

import java.io.File;
import java.io.FileNotFoundException;

public class ConfigToDTOConverter implements Converter<String, DovecotConf> {

    @Override
    public DovecotConf convert(String s) throws FileNotFoundException {
        //s is the config file location
        File file = new File(s);
        if (!file.canRead()) {
            throw new FileNotFoundException(s);
        }
        return null;
    }
}
