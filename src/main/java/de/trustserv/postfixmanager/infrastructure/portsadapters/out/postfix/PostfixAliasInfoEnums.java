package de.trustserv.postfixmanager.infrastructure.portsadapters.out.postfix;


public enum PostfixAliasInfoEnums {QUERY, HOSTS, USER, PASSWORD, DBNAME}