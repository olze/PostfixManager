package de.trustserv.postfixmanager.infrastructure.portsadapters.out.dovecot;

import java.io.FileNotFoundException;

public interface Converter<S, T> {

    T convert(S s) throws FileNotFoundException;

}
