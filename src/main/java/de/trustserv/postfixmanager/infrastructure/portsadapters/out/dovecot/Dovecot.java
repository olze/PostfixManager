package de.trustserv.postfixmanager.infrastructure.portsadapters.out.dovecot;

public final class Dovecot {

    private final DovecotConf config;

    public Dovecot(DovecotConf config) {
        this.config = config;
    }
}
