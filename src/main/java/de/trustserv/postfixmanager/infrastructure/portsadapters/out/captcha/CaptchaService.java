package de.trustserv.postfixmanager.infrastructure.portsadapters.out.captcha;

import de.trustserv.postfixmanager.application.services.TooManyRequestsException;
import de.trustserv.postfixmanagerapi.rest.commands.unauthorized.RegisterUserCommand;
import de.trustserv.postfixmanagerapi.rest.commands.unauthorized.RequestCaptchaCommand;

import javax.ws.rs.core.Response;

public interface CaptchaService {

    Response requestCaptcha(RequestCaptchaCommand cmd) throws TooManyRequestsException;

    boolean captchaCorrect(RegisterUserCommand command);
}
