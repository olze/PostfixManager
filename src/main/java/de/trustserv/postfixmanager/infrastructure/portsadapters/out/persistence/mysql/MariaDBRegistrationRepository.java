package de.trustserv.postfixmanager.infrastructure.portsadapters.out.persistence.mysql;

import de.trustserv.postfixmanager.domain.model.registration.Registration;
import de.trustserv.postfixmanager.domain.model.registration.RegistrationRepository;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Optional;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MariaDBRegistrationRepository implements RegistrationRepository {
    private final String sqlDriver = "org.mariadb.jdbc.Driver";
    private final File configFile = new File("sqlconfig.properties");
    private final String PROPERTY_SQL_USER = "sql.user";
    private final String PROPERTY_SQL_PASS = "sql.password";
    private final String PROPERTY_SQL_HOST = "sql.host";
    private final String PROPERTY_SQL_DB = "sql.db";
    private String username;
    private String password;
    private String host;
    private String db;
    private Connection connect;
    private PreparedStatement checkIfRegistrationAlreadyExist;
    private PreparedStatement saveRegistration;

    private MariaDBRegistrationRepository() {

    }

    public static MariaDBRegistrationRepository createInstance() {
        MariaDBRegistrationRepository registrationRepository = new MariaDBRegistrationRepository();
        try {
            registrationRepository.init();
        } catch (SQLException e) {
            throw new RuntimeException("creating mariadb registration repo failed: " + e.getMessage());
        }
        return registrationRepository;
    }

    @Override
    public Registration get(String mail) {
        return null;
    }

    @Override
    public Optional<Registration> find(String mail) {
        ensureConnected();
        try {
            checkIfRegistrationAlreadyExist.setString(1, mail);
            final ResultSet resultSet = checkIfRegistrationAlreadyExist.executeQuery();
            if (resultSet.first()) {
                final Timestamp acceptedAt = resultSet.getTimestamp("acceptedAt");
                final Timestamp blockedAt = resultSet.getTimestamp("blockedAt");
                final Registration registration = new Registration(resultSet.getString("email"), resultSet.getString("ip"),
                        resultSet.getTimestamp("requestedAt").toLocalDateTime(),
                        acceptedAt == null ? null : acceptedAt.toLocalDateTime(),
                        blockedAt == null ? null : blockedAt.toLocalDateTime());
                return Optional.of(registration);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    private void ensureConnected() {
        try {
            if (!connect.isValid(500)) {
                throw new SQLException("Invalid SQL Connection. Forgot to call init() ?");
            }
        } catch (SQLException ex) {
            Logger.getLogger(MariaDBUserRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void init() throws SQLException {
        final Properties sqlConfig = new Properties();
        try {
            try (FileInputStream fileInputStream = new FileInputStream(configFile)) {
                sqlConfig.load(fileInputStream);
            }
        } catch (IOException ex) {
            throw new SQLException("SQL Config not found at " + configFile.getAbsolutePath(), ex);
        }

        username = sqlConfig.getProperty(PROPERTY_SQL_USER);
        password = sqlConfig.getProperty(PROPERTY_SQL_PASS);
        host = sqlConfig.getProperty(PROPERTY_SQL_HOST);
        db = sqlConfig.getProperty(PROPERTY_SQL_DB);

        final String connectionString = "jdbc:mariadb://" + host + "/" + db;
        try {
            // This will load the MySQL driver, each DB has its own driver
            Class.forName(sqlDriver);
            // Setup the connection with the DB
            connect = DriverManager.getConnection(connectionString, username, password);
            final String sqlRegistrationAlreadyExists = "SELECT * FROM registration WHERE email = ?;";
            checkIfRegistrationAlreadyExist = connect.prepareStatement(sqlRegistrationAlreadyExists);
            final String sqlSaveRegistration = "INSERT INTO registration(email, ip, requestedAt, acceptedAt, blockedAt) " +
                    "VALUES(?, ?, ?, ?, ?);";
            saveRegistration = connect.prepareStatement(sqlSaveRegistration);
            final boolean tableCreated = connect.createStatement().execute("CREATE TABLE IF NOT EXISTS registration(" +
                    "email varchar(255) NOT NULL," +
                    "ip varchar(15) NOT NULL," +
                    "requestedAt datetime NOT NULL," +
                    "acceptedAt datetime," +
                    "blockedAt datetime);"
            );
            if (tableCreated) {
                Logger.getLogger(MariaDBUserRepository.class.getName()).log(Level.FINE, "table registration created!");
            }
        } catch (ClassNotFoundException | SQLException ex) {
            throw new SQLException("Unable to connect to the db. Please check settings and the mysql .jar file", ex);
        }
    }

    @Override
    public void save(Registration aggregate) {
        ensureConnected();

        try {
            connect.setAutoCommit(false);
            saveRegistration.setString(1, aggregate.getEmail());
            saveRegistration.setString(2, aggregate.getIp());
            saveRegistration.setTimestamp(3, Timestamp.valueOf(aggregate.getRequestedAt()));
            saveRegistration.setTimestamp(4, null);
            saveRegistration.setTimestamp(5, null);
            saveRegistration.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
