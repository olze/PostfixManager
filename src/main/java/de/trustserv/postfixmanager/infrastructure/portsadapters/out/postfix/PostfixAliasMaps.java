package de.trustserv.postfixmanager.infrastructure.portsadapters.out.postfix;

import de.trustserv.postfixmanager.infrastructure.portsadapters.out.shared.GenericInfoExtractor.QueryEntryNotFoundException;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.*;

import static de.trustserv.postfixmanager.infrastructure.portsadapters.out.postfix.PostfixAliasInfoEnums.*;
import static java.util.Objects.requireNonNull;

public class PostfixAliasMaps {

    private final PostfixAliasMapsInfo connectionInfos;
    private final Class driver;

    public PostfixAliasMaps(PostfixAliasMapsInfo connectionInfos) {
        requireNonNull(this.connectionInfos = connectionInfos);
        try {
            driver = Class.forName(connectionInfos.getFQDriver());
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public ResultSet executeQuery(String usernameToSearchFor) {
        try {
            final PostfixAliasInfoExtractor xtractor = new PostfixAliasInfoExtractor(new File(connectionInfos.getFilePath()));
            final String host = xtractor.extract(HOSTS);
            final String user = xtractor.extract(USER);
            final String password = xtractor.extract(PASSWORD);
            final String dbname = xtractor.extract(DBNAME);
            final String query = xtractor.extract(QUERY).replaceFirst("\\%s", usernameToSearchFor);
            final String connectionString = "jdbc:mariadb://" + host + "/" + dbname;
            final Connection connection = DriverManager.getConnection(connectionString, user, password);
            final PreparedStatement preparedStatement = connection.prepareStatement(query);
            final ResultSet resultSet = preparedStatement.executeQuery();
            connection.close();
            return resultSet;

        } catch (FileNotFoundException | SQLException | QueryEntryNotFoundException e) {
            throw new RuntimeException("Fatal error.", e);
        }
    }
}
