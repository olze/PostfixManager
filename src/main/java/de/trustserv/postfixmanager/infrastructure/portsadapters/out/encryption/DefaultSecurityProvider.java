package de.trustserv.postfixmanager.infrastructure.portsadapters.out.encryption;

import de.trustserv.postfixmanager.domain.model.encryption.SecurityProvider;

import java.security.*;

public class DefaultSecurityProvider implements SecurityProvider {

    private final Key privKey;
    private final Key pubKey;

    public DefaultSecurityProvider() {
        KeyPairGenerator keyGen;
        try {
            keyGen = KeyPairGenerator.getInstance("RSA");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("RSA Implementation for Keypair Generator not found!");
        }
        keyGen.initialize(2048, new SecureRandom());
        KeyPair keypair = keyGen.generateKeyPair();
        privKey = keypair.getPrivate();
        pubKey = keypair.getPublic();
    }

    @Override
    public Key getKey() {
        return privKey;
    }

}
