package de.trustserv.postfixmanager.infrastructure.portsadapters.in.web.rest;

import de.trustserv.postfixmanager.application.services.AuthenticationException;
import de.trustserv.postfixmanager.application.services.UserService;
import de.trustserv.postfixmanagerapi.rest.commands.unauthorized.LoginUserCommand;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/session")
public class SessionRessource {

    private final UserService userService;

    public SessionRessource(UserService userService) {
        this.userService = userService;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/")
    public Response loginUser(@Valid LoginUserCommand cmd) {
        String token;
        try {
            token = userService.loginUserReturnToken(cmd);
        } catch (AuthenticationException e) {
            return Response.status(Response.Status.FORBIDDEN).entity(e).build();
        }
        return Response.status(Response.Status.ACCEPTED).entity(token).build();
    }

}
