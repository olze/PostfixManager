package de.trustserv.postfixmanager.domain.model.encryption;

import java.security.Key;

public interface SecurityProvider {
    Key getKey();
}
