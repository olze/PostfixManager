package de.trustserv.postfixmanager.domain.model.registration;

import de.trustserv.postfixmanager.common.domain.model.Entity;

import java.time.LocalDateTime;

public class Registration implements Entity {

    private final String email;
    private final String ip;
    private final LocalDateTime requestedAt;
    private final LocalDateTime acceptedAt;
    private final LocalDateTime blockedAt;

    public Registration(String email, String ip, LocalDateTime requestedAt, LocalDateTime acceptedAt, LocalDateTime blockedAt) {
        this.email = email;
        this.ip = ip;
        this.requestedAt = requestedAt;
        this.acceptedAt = acceptedAt;
        this.blockedAt = blockedAt;
    }

    public String getEmail() {
        return email;
    }

    public String getIp() {
        return ip;
    }

    public LocalDateTime getRequestedAt() {
        return requestedAt;
    }

    public LocalDateTime getAcceptedAt() {
        return acceptedAt;
    }

    public LocalDateTime getBlockedAt() {
        return blockedAt;
    }
}
