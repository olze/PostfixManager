package de.trustserv.postfixmanager.domain.model.user;

import de.trustserv.postfixmanager.common.domain.model.Repository;


public interface UserRepository extends Repository<User> {

    int VALID_CREDENTIALS = 0;
    int ERROR_WRONG_PASSWORD = -1;
    int ERROR_UNKNOWN_USER = -2;

    int checkCredentials(String username, String password);

//    void doSomething() throws NotAuthorized;

    class NotAuthorized extends Exception {
    }


}
