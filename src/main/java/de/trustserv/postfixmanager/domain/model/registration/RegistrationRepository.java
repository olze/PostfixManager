package de.trustserv.postfixmanager.domain.model.registration;

import de.trustserv.postfixmanager.common.domain.model.Repository;

import java.util.Optional;

public interface RegistrationRepository extends Repository<Registration> {

    Optional<Registration> find(String mail);

}
