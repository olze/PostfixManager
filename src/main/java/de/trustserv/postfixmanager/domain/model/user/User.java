package de.trustserv.postfixmanager.domain.model.user;

import de.trustserv.postfixmanager.common.domain.model.Entity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Objects.requireNonNull;

public final class User implements Entity {

    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private final String email;
    private final String password;
    private final Pattern pattern = Pattern.compile(EMAIL_PATTERN);
    private final String name;

    public User(String email, String password, String name) throws InvalidEmailException {
        requireNonNull(this.email = email);
        requireNonNull(this.password = password);
        requireNonNull(this.name = name);
        validateEmail(email);
    }

    private void validateEmail(String email) throws InvalidEmailException {
        final Matcher matcher = pattern.matcher(email);
        if (!matcher.matches()) {
            throw new InvalidEmailException(email);
        }
    }

    public String getMail() {
        return email;
    }

    public String getQuota() {
        return "0";
    }

    public String getMaildir() {
        return "trustserv.de/" + getMail().split("@")[0];
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getLocalPart() {
        return getMail().split("@")[0];
    }

    public String getDomain() {
        return "trustserv.de";
    }

    private static class InvalidEmailException extends RuntimeException {
        public InvalidEmailException(String mail) {
            super(mail);
        }
    }

}
